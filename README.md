# Touch Surgery - QA Assignment

---

### Introduction

Below is a list of business requirements for a new high-tech entertainment device. Using this list and the below technical architecture diagram, please provide us with a critique of the requirements from a test perspective and a short description of the associated test strategy. Send us back a PDF file with you report.

### Technical architecture diagram

![image1](images/1.png)

### Product High-level Requirements List

1. User must be able to shoot ducks and the basic game screen displayed by the Television should look like this:  
![image2](images/2.png). 
2. When the user pulls the trigger on the gun, the screen will display as black with white rectangles in in the position of the ducks like so:  
![image3](images/3.png). 
3. If the light-gun sensor detects one of the white rectangles, the console will register this as a hit and display an image of the current state of the ducks like the one below:  
![image4](images/4.png). 
4. The game will consist of multiple rounds with increasing difficulty
5. User is allowed 3 chances to shoot a total of 2 ducks in each round
6. If the User misses either of the 2 ducks, the Dog should laugh at them
7. If the User hits both ducks, display the text ‘Nice Shooting!’
8. Our System needs to Time the User actions
9. Reward good Users
10. After each round, the Time Limit should be reduced viii.There should be more than one kind of Duck

### Problem Report Handling

While working on Duck Hunt, you get a report of a problem from the customer service team.  
Answer the questions below based on the report and your understanding of the system as outlined in the diagrams and requirements above.

1. What’s the bug description you’d write for this?
2. Can you identify the failing component? What information about this problem can you glean from these reports and how do you think you’d move the investigation forward?

##### Customer Service Report
1. From the customer: “The game won’t work, I can’t hit any ducks.”
2. When asked what happens when they try: “They just fly away every time. The Dog keeps laughing at me. It hurts my feelings.”
3. When asked what happens when they pull the light-gun trigger during the game: “I see some white boxes flash around the ducks”